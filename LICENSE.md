# Arch wallpaper

[![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)](https://creativecommons.org/publicdomain/zero/1.0/)

This work by [WarringtonSeale](http://bitbucket.org/WarringtonSeale) is licensed
[CC0](https://creativecommons.org/publicdomain/zero/1.0/). Zero rights reserved.